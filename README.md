# ESP8266 Remote LCD
An ESP8266 based display that shows the payload of specified MQTT messages
formatted with prefix and suffix strings. Message payloads can be displayed
as-is or parsed as JSON and a single value be shown. The provided firmware
code is written for the Arduino IDE and tested on a NodeMCU clone.


## Requirements
To build and flash this firmware you will need the
[Arduino IDE](https://www.arduino.cc/en/Main/Software) set up with the [ESP8266
boards](https://arduino-esp8266.readthedocs.io/en/latest/installing.html).
After setting up the IDE and the board manager URL, select your target board
from the "Boards" menu within the IDE.


## Hardware
The hardware is a 20x4 Hitachi LCD attached to an I2C backpack located on pins
GPIO4 (SDA), GPIO5 (SCL) at address 0x3F. A button is attached between GPIO0
and ground that controls the backlight (on NodeMCU this is the builtin FLASH
button). You can set another address for the LCD by changing `LCD_ADDRESS` in
`firmware/hardware.h`. It is also possible to use a 2-line LCD (16x2) without
modifying any code -- lines 3 and 4 just won't be displayed.


## Usage
The device operates in one of 2 modes at startup: **config mode** or
**client mode**. Config mode hosts an access point for directly connecting to
the device while client mode connects to an external WiFi access point and
subscribes to messages on an MQTT broker. Both modes provide a web server that
allows configuration of device settings from a browser.

### Config Mode
After the initial firmware flash the device will detect is has an invalid
configuration and perform a hard reset (all stored settings will be defaults).
The device will then start config mode and host a WiFi access point named
`LCD:<mac_address>` with the password `password`. Connect to this AP and
browse to [http://192.168.1.1/](http://192.168.1.1/) to access the
configuration interface.

<div align="center">
    <img src="screenshot.png" alt="Web interface" width="640px">
</div>

Note: After saving settings the device will be set to boot into client mode.
To return to config mode reset the device 3 consecutive times. The on-board
LED will blink 3 times to confirm the mode change.

### Client Mode
Once the configuration has been saved the device will be set to boot into
client mode. In this mode the device will connect to an MQTT broker and
subscribe to topics it can display. When a topic is recieved it will be
displayed on the LCD line associated with the topic.

The built in FLASH button will momentarily turn on the backlight for the LCD.
The builtin LED is used to announce status: a fast continuous blink indicates
a connection to WiFi or MQTT broker is in progress, and a short single blink
indicates a publish has occured.

The topic "&lt;prefix&gt;/status" is published "online" at connection and
"offline" using the LWT topic. This topic can be used to determine if the
device is currently connected and ready to recieve messages. The IP address
of the device is published to "&lt;prefix&gt;/ip" on connection.

### "Factory" Reset
To reset all settings to default values and boot into config mode, reset the
device 5 consecutive times. The on-board LED will blink 5 times to confirm the
reset.


## License
Unless otherwise noted the source in this repository is licensed under the
2-clause BSD license, see `LICENSE` for details.

The included library [PubSubClient](https://pubsubclient.knolleary.net/) is
licensed under the MIT license, see `firmware/src/pubsubclient/LICENSE.txt`
for details.

The included library
[LiquidCrystal PCF8574](https://github.com/mathertel/LiquidCrystal_PCF8574) is
licensed under the 3-clause BSD license, see
`firmware/src/liquidcrystal-pcf8574/LICENSE` for details.

The included library [ArduinoJson](https://github.com/bblanchon/ArduinoJson) is
licensed under the MIT license, see `firmware/src/arduinojson/LICENSE.md`
for details.

Note: Arduino libraries **statically linked** by this firmware are licensed
under LGPL.
