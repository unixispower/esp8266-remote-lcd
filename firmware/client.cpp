#include "client.h"
#include "eeprom.h"
#include "firmware.h"
#include "hardware.h"

#include <cstdio>
#include <cstring>
#include <cstdarg>

#include <ESP8266httpUpdate.h>


// period to wait when retrying MQTT connection
const uint8_t CONNECT_DELAY_SECS = 5;

// period to wait in loop (reduces power usage)
const uint16_t LOOP_DELAY_MILLIS = 5;

// use fixed-length buffers long enough to hold longest topic/payload
// if increasing these you may need to increase MQTT_MAX_PACKET_SIZE
// in src/pubsubclient/PubSubClient.h
const size_t MAX_TOPIC_SIZE = CONFIG_STRLEN(mqttTopic) + 32 + 2;  // + 2 = '/' & '\0'
const size_t MAX_PUBLISH_SIZE = 64 + 1;  // + 1 = '\0'
const size_t MAX_SELECTOR_SIZE = CONFIG_STRLEN(lcdLineValues[0].selector) + 1;
const size_t JSON_DOCUMENT_SIZE = 1024;

// prefix of errors printed to LCD
const char* LCD_ERROR_PREFIX = "! ";

// JSON "selector" separator character
const char* JSON_SELECTOR_SEPARATOR = ".";


MqttClient::MqttClient()
    : wifiClient(wifiClient), pubSubClient(wifiClient) {}

void MqttClient::setup() {
    this->configValues = EepromStore.getConfigPtr();
    this->backlightOnTime = 0;

    // set up MQTT client
    this->pubSubClient.setServer(this->configValues->mqttHost, this->configValues->mqttPort);
    this->pubSubClient.setCallback(
        [this](char* topic, uint8_t* payload, unsigned int length) {
            this->onMessage(topic, payload, length);
        });
}

void MqttClient::loop() {
    // (re)connect to MQTT broker
    while (!this->pubSubClient.connected()) {
        // show connecting status
        if (WiFi.status() != WL_CONNECTED) {
            Lcd.clear();
            Lcd.print("Joining WiFi");
        }

        // wait for WiFi connection
        while (WiFi.status() != WL_CONNECTED) {
            BuiltinLed.blinkFast(1);
        }

        // show connecting status
        Lcd.clear();
        Lcd.print("Joining broker");

        // some sensor values are published as retained so that newly connected
        // subscribers recieve most-recent readings for all sensors in the array.
        // LWT is used to set the status of a sensor to "offline" in the case of
        // a disconnect -- this allows disconnected sensor readings to be ignored
        char statusTopic[MAX_TOPIC_SIZE];
        this->createTopic(statusTopic, "status");

        // attempt to connect
        bool connected = this->pubSubClient.connect(
            this->configValues->mqttClientId, // client ID
            this->configValues->mqttUsername, // username
            NULL,                             // password
            statusTopic,                      // LWT topic
            0,                                // LWT QoS
            true,                             // LWT retained
            "offline");                       // LWT message

        if (connected) {
            Lcd.clear();
            uint32_t now = millis();

            // if connection takes long enough the backlight will turn off just before
            // readings are shown. this resets the timer so the backligt stays on for
            // the initial readings (this will only extend the backlight -- not turn it on)
            this->backlightOnTime = now;

            // reset all line times
            for (uint8_t i = 0; i < LCD_ROWS; ++i) {
                this->lineTimes[i] = now;
            }

            // announce online
            this->publishTopic("status", true, "online");
            this->publishTopic("ip", true, WiFi.localIP().toString().c_str());

            // subscribe to LCD topics
            for (uint8_t i = 0; i < LCD_ROWS; ++i) {
                const lcd_line_values* lineConfig = &this->configValues->lcdLineValues[i];
                if (strlen(lineConfig->topic) != 0) {
                    this->pubSubClient.subscribe(lineConfig->topic);
                }
            }
        } else {
            // delay longer to avoid spamming the network
            BuiltinLed.blinkFastForSecs(CONNECT_DELAY_SECS);
        }
    }

    // enable backlight controls if a duration is set
    if (this->configValues->lcdBacklightSecs != 0) {
        // turn on backlight if button is pressed
        if(digitalRead(BACKLIGHT_BUTTON_PIN) == LOW) {
            this->backlightOnTime = millis();
            Lcd.setBacklight(255);
        }

        // turn off backlight after BACKLIGHT_DURATION_SECS elapses
        uint32_t elapsedMillis = millis() - this->backlightOnTime;
        if (elapsedMillis >= this->configValues->lcdBacklightSecs * 1000) {
            Lcd.setBacklight(0);
        }
    }

    this->pubSubClient.loop();
    this->checkTimeouts();
    delay(LOOP_DELAY_MILLIS);
}

// create a topic string by appending a subtopic to the configured topic
void MqttClient::createTopic(char buffer[], const char* subTopic) {
    if (strlen(this->configValues->mqttTopic) == 0) {
        // copy subtopic directly if no topic is specified
        strcpy(buffer, subTopic);
    } else {
        sprintf(buffer, "%s/%s", this->configValues->mqttTopic, subTopic);
    }
}

// publish a topic by specifying a subtopic, format string, and varargs like with sprintf
void MqttClient::publishTopic(const char* subTopic, boolean retained, const char* format, ...) {
    char topic[MAX_TOPIC_SIZE];
    this->createTopic(topic, subTopic);

    // use vprintf to handle format string and variadic arguments
    char payload[MAX_PUBLISH_SIZE];
    va_list args;
    va_start(args, format);
    vsprintf(payload, format, args);
    va_end(args);

    // publish
    this->pubSubClient.publish(topic, payload, retained);
}

// handle and incoming message
void MqttClient::onMessage(char* topic, uint8_t* payload, size_t length) {
    uint32_t now = millis();

    // attempt to parse the payload as JSON no matter what
    // this prevents multiple parses if more than one line matches the same topic
    StaticJsonDocument<JSON_DOCUMENT_SIZE> jsonDocument;
    DeserializationError jsonError = deserializeJson(jsonDocument, (char*)payload, length);

    for (uint8_t i = 0; i < LCD_ROWS; ++i) {
        const lcd_line_values* lineConfig = &this->configValues->lcdLineValues[i];

        // skip line that doesnt match recieved topic
        if (strcmp(topic, lineConfig->topic) != 0) {
            continue;
        }

        // record line update time
        this->lineTimes[i] = now;

        // determine type of payload based on selector presence
        if (strlen(lineConfig->selector) != 0) {
            // print error if JSON could not be parsed
            if (jsonError) {
                const char* error = NULL;
                switch(jsonError.code()) {
                    case DeserializationError::IncompleteInput:
                    case DeserializationError::InvalidInput:
                    case DeserializationError::NotSupported:
                        error = "JSON invalid";
                        break;
                    case DeserializationError::NoMemory:
                        error = "JSON too big";
                        break;
                    case DeserializationError::TooDeep:
                        error = "JSON too deep";
                        break;
                    default:
                        error = "JSON error";
                        break;
                }
                this->printLcdLine(i, LCD_ERROR_PREFIX, error, "", 0);
                continue;
            }

            // extract JSON value as string
            char valueString[LCD_LINE_LENGTH + 1];
            bool found = extractJsonValue(&jsonDocument, lineConfig->selector,
                valueString, sizeof(valueString));

            // print an error if the value wasn't found
            if (!found) {
                this->printLcdLine(i, LCD_ERROR_PREFIX, "Value missing", "", 0);
                continue;
            }

            // print JSON value
            this->printLcdLine(i, lineConfig->prefix, lineConfig->suffix,
                valueString, strlen(valueString));

        } else {
            // print raw value
            this->printLcdLine(i, lineConfig->prefix, lineConfig->suffix,
                (const char*)payload, length);
        }
    }

    BuiltinLed.blip();
}

// extract the selected value from a JSON document and return true if successful
bool MqttClient::extractJsonValue(JsonDocument* jsonDocument, const char* selector,
        char* buffer, size_t size) {
    // strtok is destructive -- tokenize a copy of the selector
    char selectorCopy[MAX_SELECTOR_SIZE];
    strcpy(selectorCopy, selector);

    // traverse the JSON structure one selector token at a time
    // this assumes the structure is entirely objects -- arrays are not supported
    JsonVariant tokenVariant = jsonDocument->as<JsonVariant>();
    char* selectorToken = strtok(selectorCopy, JSON_SELECTOR_SEPARATOR);
    while (!tokenVariant.isNull() && selectorToken != NULL) {
        // convert the previous variant to an object and extract the current one
        tokenVariant = tokenVariant.getMember(selectorToken);
        // find next token
        selectorToken = strtok(NULL, JSON_SELECTOR_SEPARATOR);
    }

    if (tokenVariant.isNull()) {
        return false;
    }

    // convert the type to a string
    if (tokenVariant.is<char*>()) {
        strncpy(buffer, tokenVariant.as<char*>(), size);
        buffer[size - 1] = '\0';
    } else if (tokenVariant.is<int>()) {
        long valueLong = tokenVariant.as<long>();
        snprintf(buffer, size, "%ld", valueLong);
    } else if (tokenVariant.is<double>()) {
        double valueDouble = tokenVariant.as<double>();
        snprintf(buffer, size, "%g", valueDouble);
    } else {
        return false;
    }

    return true;
}

// write a payload to a line of the LCD
void MqttClient::printLcdLine(uint8_t row, const char* prefix, const char* suffix,
        const char* payload, size_t length) {
    char line[LCD_LINE_LENGTH + 1];

    // set line to a string of spaces so all previous characters are replaced or erased
    memset(line, ' ', sizeof(line) - 1);
    line[sizeof(line) - 1] = '\0';

    // copy payload into line and erase null character written by sprintf
    int count = snprintf(line, sizeof(line), "%s%.*s%s", prefix, length, payload, suffix);
    if (count >= 0 && count < sizeof(line) - 1) {
        line[count] = ' ';  // replace null character with space
    }

    Lcd.setCursor(0, row);
    Lcd.print(line);
}

// check all line times and show errors for ones that have timed out
void MqttClient::checkTimeouts() {
    uint32_t now = millis();

    for (uint8_t i = 0; i < LCD_ROWS; ++i) {
        const lcd_line_values* lineConfig = &this->configValues->lcdLineValues[i];

        // skip lines that don't have a topic or have timeout disabled
        if (strlen(lineConfig->topic) == 0 || lineConfig->timeoutSecs == 0) {
            continue;
        }

        // print an error if timed out
        uint32_t elapsedMillis = now - this->lineTimes[i];
        if (elapsedMillis >= lineConfig->timeoutSecs * 1000) {
            this->printLcdLine(i, LCD_ERROR_PREFIX, "Timeout", "", 0);
        }
    }
}
