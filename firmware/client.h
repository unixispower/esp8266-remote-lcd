/**
 * MQTT client that publishes sensor updates.
 */
#ifndef CLIENT_H
#define CLIENT_H

#include "config.h"

#include "src/pubsubclient/PubSubClient.h"
#include "src/arduinojson/ArduinoJson.h"

#include <ESP8266WiFi.h>


/**
 * MQTT client that publishes sensor updates.
 */
class MqttClient {
    public:
        MqttClient();
        void setup();
        void loop();

    private:
        const config_values* configValues;
        uint32_t backlightOnTime;
        uint32_t lineTimes[LCD_ROWS];
        WiFiClient wifiClient;
        PubSubClient pubSubClient;
        void createTopic(char buffer[], const char* subTopic);
        void publishTopic(const char* subTopic, boolean retained, const char* format, ...);
        void onMessage(char* topic, uint8_t* payload, size_t length);
        bool extractJsonValue(JsonDocument* jsonDocument, const char* selector,
            char* buffer, size_t size);
        void printLcdLine(uint8_t row, const char* prefix, const char* suffix,
            const char* payload, size_t length);
        void checkTimeouts();
};

#endif
