#include "config.h"

#include <cctype>
#include <cstdio>
#include <cstring>
#include <cstdarg>


// prefixes of LCD fields
#define LCD_TOPIC_PREFIX    "lcd-topic-"
#define LCD_SELECTOR_PREFIX "lcd-selector-"
#define LCD_PREFIX_PREFIX   "lcd-prefix-"
#define LCD_SUFFIX_PREFIX   "lcd-suffix-"
#define LCD_TIMEOUT_PREFIX  "lcd-timeout-"


// names of user fields
const char WIFI_HOSTNAME[]  = "wifi-hostname";
const char WIFI_SSID[]      = "wifi-ssid";
const char WIFI_PASSWORD[]  = "wifi-password";
const char MQTT_HOST[]      = "mqtt-host";
const char MQTT_PORT[]      = "mqtt-port";
const char MQTT_USERNAME[]  = "mqtt-username";
const char MQTT_CLIENT_ID[] = "mqtt-client-id";
const char MQTT_TOPIC[]     = "mqtt-topic";
const char LCD_BACKLIGHT[]  = "lcd-backlight";

// list of all names defined above
const char* const StringConfig::USER_FIELDS[] = {
    WIFI_HOSTNAME,
    WIFI_SSID,
    WIFI_PASSWORD,
    MQTT_HOST,
    MQTT_PORT,
    MQTT_USERNAME,
    MQTT_CLIENT_ID,
    MQTT_TOPIC,
    LCD_BACKLIGHT,

    LCD_TOPIC_PREFIX    "1",
    LCD_SELECTOR_PREFIX "1",
    LCD_PREFIX_PREFIX   "1",
    LCD_SUFFIX_PREFIX   "1",
    LCD_TIMEOUT_PREFIX  "1",

    LCD_TOPIC_PREFIX    "2",
    LCD_SELECTOR_PREFIX "2",
    LCD_PREFIX_PREFIX   "2",
    LCD_SUFFIX_PREFIX   "2",
    LCD_TIMEOUT_PREFIX  "2",

    LCD_TOPIC_PREFIX    "3",
    LCD_SELECTOR_PREFIX "3",
    LCD_PREFIX_PREFIX   "3",
    LCD_SUFFIX_PREFIX   "3",
    LCD_TIMEOUT_PREFIX  "3",

    LCD_TOPIC_PREFIX    "4",
    LCD_SELECTOR_PREFIX "4",
    LCD_PREFIX_PREFIX   "4",
    LCD_SUFFIX_PREFIX   "4",
    LCD_TIMEOUT_PREFIX  "4",
};

// error message templates
const char ERROR_TEMP_REQUIRED[]    = "%s is required.";
const char ERROR_TEMP_REQUIRED_BY[] = "%s is required when %s is set.";
const char ERROR_TEMP_ASCII[]       = "%s must only contain printable ASCII characters.";
const char ERROR_TEMP_TOO_LONG[]    = "%s must not be longer than %u characters.";
const char ERROR_TEMP_NUMBER[]      = "%s must be a number in the range %u to %u.";
const char ERROR_TEMP_UNKNOWN[]     = "Unknown field \"%s\".";

const size_t StringConfig::USER_FIELD_COUNT =
        sizeof(StringConfig::USER_FIELDS) / sizeof(StringConfig::USER_FIELDS[0]);

StringConfig::StringConfig(config_values* values)
        : values(values) {}

const char* StringConfig::format(const char* field) {
    unsigned char lcdLineNumber;

    // WiFi
    if (strcmp(field, WIFI_HOSTNAME) == 0) {
        return this->values->wifiHostname;
    } else if (strcmp(field, WIFI_SSID) == 0) {
        return this->values->wifiSsid;
    } else if (strcmp(field, WIFI_PASSWORD) == 0) {
        return this->values->wifiPassword;
    }

    // MQTT
    else if (strcmp(field, MQTT_HOST) == 0) {
        return this->values->mqttHost;
    } else if (strcmp(field, MQTT_PORT) == 0) {
        return this->bprintf("%u", this->values->mqttPort);
    } else if (strcmp(field, MQTT_USERNAME) == 0) {
        return this->values->mqttUsername;
    } else if (strcmp(field, MQTT_CLIENT_ID) == 0) {
        return this->values->mqttClientId;
    } else if (strcmp(field, MQTT_TOPIC) == 0) {
        return this->values->mqttTopic;
    }

    // LCD
    else if (strcmp(field, LCD_BACKLIGHT) == 0) {
        return this->bprintf("%u", this->values->lcdBacklightSecs);
    }

    // LCD lines
    else if (sscanf(field, LCD_TOPIC_PREFIX "%hhu", &lcdLineNumber) == 1) {
        if (lcdLineNumber > 0 && lcdLineNumber <= LCD_ROWS) {
            return this->values->lcdLineValues[lcdLineNumber - 1].topic;
        }
        return this->bprintf("");  // field not found

    } else if (sscanf(field, LCD_SELECTOR_PREFIX "%hhu", &lcdLineNumber) == 1) {
        if (lcdLineNumber > 0 && lcdLineNumber <= LCD_ROWS) {
            return this->values->lcdLineValues[lcdLineNumber - 1].selector;
        }
        return this->bprintf("");  // field not found

    } else if (sscanf(field, LCD_PREFIX_PREFIX "%hhu", &lcdLineNumber) == 1) {
        if (lcdLineNumber > 0 && lcdLineNumber <= LCD_ROWS) {
            return this->values->lcdLineValues[lcdLineNumber - 1].prefix;
        }
        return this->bprintf("");  // field not found

    } else if (sscanf(field, LCD_SUFFIX_PREFIX "%hhu", &lcdLineNumber) == 1) {
        if (lcdLineNumber > 0 && lcdLineNumber <= LCD_ROWS) {
            return this->values->lcdLineValues[lcdLineNumber - 1].suffix;
        }
        return this->bprintf("");  // field not found

    } else if (sscanf(field, LCD_TIMEOUT_PREFIX "%hhu", &lcdLineNumber) == 1) {
        if (lcdLineNumber > 0 && lcdLineNumber <= LCD_ROWS) {
            return this->bprintf("%u", this->values->lcdLineValues[lcdLineNumber - 1].timeoutSecs);
        }
        return this->bprintf("");  // field not found
    }

    // all other fields
    else {
        return this->bprintf("");  // field not found
    }
}

const char* StringConfig::parse(const char* field, const char* value) {
    unsigned char lcdLineNumber;

    // WiFi
    if (strcmp(field, WIFI_HOSTNAME) == 0) {
        return this->parseString("Wifi hostname", value, this->values->wifiHostname, CONFIG_STRLEN(wifiHostname));
    } else if (strcmp(field, WIFI_SSID) == 0) {
        return this->parseString("Wifi SSID", value, this->values->wifiSsid, CONFIG_STRLEN(wifiSsid));
    } else if (strcmp(field, WIFI_PASSWORD) == 0) {
        return this->parseString("Wifi password", value, this->values->wifiPassword, CONFIG_STRLEN(wifiPassword));
    }

    // MQTT
    else if (strcmp(field, MQTT_HOST) == 0) {
        return this->parseString("MQTT host", value, this->values->mqttHost, CONFIG_STRLEN(mqttHost));
    } else if (strcmp(field, MQTT_PORT) == 0) {
        long result;
        const char* error = this->parseNumber("MQTT port", value, &result, 1, UINT16_MAX);
        if (error != NULL) { return error; }
        this->values->mqttPort = result;
    } else if (strcmp(field, MQTT_USERNAME) == 0) {
        return this->parseString("MQTT username", value, this->values->mqttUsername, CONFIG_STRLEN(mqttUsername));
    } else if (strcmp(field, MQTT_CLIENT_ID) == 0) {
        return this->parseString("MQTT client ID", value, this->values->mqttClientId, CONFIG_STRLEN(mqttClientId));
    } else if (strcmp(field, MQTT_TOPIC) == 0) {
        return this->parseString("MQTT topic", value, this->values->mqttTopic, CONFIG_STRLEN(mqttTopic));
    }

    // LCD
    else if (strcmp(field, LCD_BACKLIGHT) == 0) {
        long result;
        const char* error = this->parseNumber("LCD backlight", value, &result, 0, UINT8_MAX);
        if (error != NULL) { return error; }
        this->values->lcdBacklightSecs = result;
    }

    // LCD lines
    else if (sscanf(field, LCD_TOPIC_PREFIX "%hhu", &lcdLineNumber) == 1) {
        if (lcdLineNumber > 0 && lcdLineNumber <= LCD_ROWS) {
            return this->parseString("LCD line topic", value,
                this->values->lcdLineValues[lcdLineNumber - 1].topic,
                CONFIG_STRLEN(lcdLineValues[0].topic));
        }
        return this->bprintf("Invalid LCD line %u", lcdLineNumber);

    } else if (sscanf(field, LCD_SELECTOR_PREFIX "%hhu", &lcdLineNumber) == 1) {
        if (lcdLineNumber > 0 && lcdLineNumber <= LCD_ROWS) {
            return this->parseString("LCD line selector", value,
                this->values->lcdLineValues[lcdLineNumber - 1].selector,
                CONFIG_STRLEN(lcdLineValues[0].selector));
        }
        return this->bprintf("Invalid LCD line %u", lcdLineNumber);

    } else if (sscanf(field, LCD_PREFIX_PREFIX "%hhu", &lcdLineNumber) == 1) {
        if (lcdLineNumber > 0 && lcdLineNumber <= LCD_ROWS) {
            return this->parseString("LCD line prefix", value,
                this->values->lcdLineValues[lcdLineNumber - 1].prefix,
                CONFIG_STRLEN(lcdLineValues[0].prefix));
        }
        return this->bprintf("Invalid LCD line %u", lcdLineNumber);

    } else if (sscanf(field, LCD_SUFFIX_PREFIX "%hhu", &lcdLineNumber) == 1) {
        if (lcdLineNumber > 0 && lcdLineNumber <= LCD_ROWS) {
            return this->parseString("LCD line suffix", value,
                this->values->lcdLineValues[lcdLineNumber - 1].suffix,
                CONFIG_STRLEN(lcdLineValues[0].suffix));
        }
        return this->bprintf("Invalid LCD line %u", lcdLineNumber);

    } else if (sscanf(field, LCD_TIMEOUT_PREFIX "%hhu", &lcdLineNumber) == 1) {
        if (lcdLineNumber > 0 && lcdLineNumber <= LCD_ROWS) {
            long result;
            const char* error = this->parseNumber("LCD line timeout", value, &result, 0, UINT16_MAX);
            if (error != NULL) { return error; }
            this->values->lcdLineValues[lcdLineNumber - 1].timeoutSecs = result;
            return NULL;
        }
        return this->bprintf("Invalid LCD line %u", lcdLineNumber);
    }

    // unknown field
    else {
        return this->bprintf(ERROR_TEMP_UNKNOWN, field);
    }

    // success
    return NULL;
}

const char* StringConfig::isValid() {
    // WiFi
    if (strlen(this->values->wifiHostname) == 0) {
        return this->bprintf(ERROR_TEMP_REQUIRED, "WiFi hostname");
    } if (strlen(this->values->wifiSsid) == 0) {
        return this->bprintf(ERROR_TEMP_REQUIRED, "WiFi SSID");
    } if (strlen(this->values->wifiPassword) == 0) {
        return this->bprintf(ERROR_TEMP_REQUIRED, "WiFi password");
    }

    // MQTT
    if (strlen(this->values->mqttHost) == 0) {
        return this->bprintf(ERROR_TEMP_REQUIRED, "MQTT host");
    } if (this->values->mqttPort == 0) {
        return this->bprintf(ERROR_TEMP_REQUIRED, "MQTT port");
    } if (strlen(this->values->mqttClientId) == 0) {
        return this->bprintf(ERROR_TEMP_REQUIRED, "MQTT client ID");
    }

    // LCD lines
    for (uint8_t i = 0; i < LCD_ROWS; ++i) {
        const lcd_line_values* lineConfig = &this->values->lcdLineValues[i];

        if (strlen(lineConfig->topic) == 0 && strlen(lineConfig->selector) != 0) {
            return this->bprintf(ERROR_TEMP_REQUIRED_BY, "LCD line topic", "LCD line selector");
        } if (strlen(lineConfig->topic) == 0 && strlen(lineConfig->prefix) != 0) {
            return this->bprintf(ERROR_TEMP_REQUIRED_BY, "LCD line topic", "LCD line prefix");
        } if (strlen(lineConfig->topic) == 0 && strlen(lineConfig->suffix) != 0) {
            return this->bprintf(ERROR_TEMP_REQUIRED_BY, "LCD line topic", "LCD line suffix");
        }
    }

    // all fields valid
    return NULL;
}

// parse a string field or return an error message on failure
const char* StringConfig::parseString(const char* title, const char* value, char* configDest, size_t maxLength) {
    if (strlen(value) > maxLength) {
        return this->bprintf(ERROR_TEMP_TOO_LONG, title, maxLength);
    } if (!this->isPrintableString(value)) {
        return this->bprintf(ERROR_TEMP_ASCII, title);
    }
    strcpy(configDest, value);
    return NULL;
}

// parse a number field or return an error message on failure
const char* StringConfig::parseNumber(const char* title, const char* value, long* result, long minValue, long maxValue) {
    int count = sscanf(value, "%ld", result);
    if (count != 1 || *result < minValue || *result > maxValue) {
        return this->bprintf(ERROR_TEMP_NUMBER, title, minValue, maxValue);
    }
    return NULL;
}

// sprintf to internal buffer and return pointer to it
const char* StringConfig::bprintf(const char* format, ...) {
    va_list args;
    va_start(args, format);
    vsprintf(this->buffer, format, args);
    va_end(args);
    return this->buffer;
}

// indicate if value is a printable ASCII string
bool StringConfig::isPrintableString(const char* value) {
    for (size_t i = 0, length = strlen(value); i < length; ++i) {
        if (!isprint(value[i])) {
            return false;
        }
    }
    return true;
}
