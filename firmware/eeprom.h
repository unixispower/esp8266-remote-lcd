/**
 * Configuration storage in EEPROM.
 */
// Arduino std lib uses "EEPROM_H"
#ifndef EEPROM_H_
#define EEPROM_H_

#include "config.h"
#include "firmware.h"

#include <cstdint>
#include <cstddef>


/**
 * Default eeprom_values values.
 */
#define EEPROM_DEFAULT_VALUES {                 \
    "",                     /* hash */          \
    FIRMWARE_NAME,          /* name */          \
    FIRMWARE_VERSION,       /* version */       \
    MODE_CONFIG,            /* mode */          \
    sizeof(config_values),  /* configSize */    \
    CONFIG_DEFAULT_VALUES   /* configValues */  \
}


const size_t EEPROM_HASH_SIZE = 16;        // length of MD5 in bytes
const size_t EEPROM_NAME_SIZE = 32 + 1;    // length firmware name string buffer
const size_t EEPROM_VERSION_SIZE = 16 + 1; // length firmware version string buffer

/**
 * Config mode -- start a web server for configuring device.
 */
const uint8_t MODE_CONFIG = 0;

/**
 * Client mode -- connect to an MQTT broker and publish updates.
 */
const uint8_t MODE_CLIENT = 1;


/**
 * Structure containing all EEPROM values.
 */
typedef struct eeprom_values {
    uint8_t hash[EEPROM_HASH_SIZE];     // hash of all EEPROM values
    char name[EEPROM_NAME_SIZE];        // firmware name
    char version[EEPROM_VERSION_SIZE];  // firmware version
    uint8_t mode;                       // startup mode
    uint16_t configSize;                // size in bytes of stored config_values
    config_values configValues;         // values from config.h
} eeprom_values;



/**
 * Loads and saves values in "EEPROM" (SPI Flash on the ESP8266).
 * Other modules should use the "EepromStoreClass" instance provided by this header.
 */
class EepromStoreClass {
    public:
        EepromStoreClass();

        /**
         */
        void begin();

        /**
         * Commit the EEPROM buffer to flash memory.
         */
        void commit();

        /**
         * Get the config mode from the EEPROM buffer.
         */
        uint8_t getMode();

        /**
         * Set the mode in the EEPROM buffer.
         */
        void setMode(uint8_t mode);

        /**
         * Get a pointer to the config_values in the EEPROM buffer.
         */
        const config_values* getConfigPtr();

        /**
         * Copy config values from the EEPROM buffer.
         */
        void getConfig(config_values* values);

        /**
         * Copy config values to the EEPROM buffer.
         */
        void setConfig(const config_values* values);

        /**
         * Commit the default EEPROM values to flash memory.
         */
        void writeDefaults();

    private:
        eeprom_values* getBufferPtr();
        bool isValid();
        void migrate();
        void calcHash(uint8_t* hash);
};


/**
 * Global instance used by other modules.
 */
extern EepromStoreClass EepromStore;

#endif
