/**
 * Firmware information.
 */
#ifndef FIRMWARE_H
#define FIRMWARE_H

#define FIRMWARE_TITLE        "ESP8266 Remote LCD"
#define FIRMWARE_SHORT_TITLE  "Remote LCD"
#define FIRMWARE_NAME         "esp8266-remote-lcd"
#define FIRMWARE_VERSION      "0.4.0"

#endif
