#include "hardware.h"

#include <cstdint>

#include <Arduino.h>


void hardware_setup() {
    BuiltinLed.begin();

    pinMode(BACKLIGHT_BUTTON_PIN, INPUT_PULLUP);

    Lcd.begin(LCD_COLUMNS, LCD_ROWS);
    Lcd.clear();
    Lcd.setBacklight(255);  // on
}


Led BuiltinLed(BUILTIN_LED_PIN);
LiquidCrystal_PCF8574 Lcd(LCD_ADDRESS);
