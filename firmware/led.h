/**
 * Configuration storage in EEPROM.
 */
#ifndef LED_H
#define LED_H

#include <cstdint>


/**
 * Convenience for blinking an LED using delay().
 * This class assumes inverted logic for the ESP8266.
 */
class Led {
    public:
        Led(uint8_t pin);

        /**
         * Set pin direction.
         */
        void begin();

        /**
         * Turn LED on.
         */
        void on();

        /**
         * Turn LED off.
         */
        void off();

        /**
         * Blink count times at a rate that can be counted by a human.
         */
        void blink(uint8_t count);

        /**
         * Blink count times at a fast rate.
         */
        void blinkFast(uint8_t count);

        /**
         * Blink at a fast rate for *at least* the specified number of seconds.
         */
        void blinkFastForSecs(uint8_t seconds);

        /**
         * Blink once for a very short period.
         */
        void blip();

    private:
        uint8_t pin;
        void blinkDelay(uint8_t count, uint16_t delayMillis);
};

#endif
