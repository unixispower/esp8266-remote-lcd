#include "reset.h"

#include <Esp.h>
#include <Arduino.h>


// period after reset that another consecutive reset will be counted.
const uint16_t PERIOD_MILLIS = 1000;

// RTC memory offset.
const size_t RESET_OFFSET = 0;

// bitmask used to extract reset count from read word.
const uint32_t COUNT_MASK = 0x000000FF;

// pattern indicating the word contains a valid count.
const uint32_t WORD_PATTERN = 0xABCDEF00;


ResetDetectorClass::ResetDetectorClass() {}

uint8_t ResetDetectorClass::count() {
    // read reset word
    uint32_t resetWord;
    ESP.rtcUserMemoryRead(RESET_OFFSET, &resetWord, sizeof(resetWord));

    // exctract count and compare pattern bits to determine if count is valid
    uint8_t totalCount = resetWord & COUNT_MASK;
    bool isCountValid = (resetWord & ~COUNT_MASK) == WORD_PATTERN;

    if (isCountValid) {
        // count is valid -- increment
        resetWord = WORD_PATTERN | ++totalCount;
    } else {
        // count is invalid -- set to 0 to indicate cold boot
        resetWord = WORD_PATTERN;
    }

    // store new count in RTC
    ESP.rtcUserMemoryWrite(RESET_OFFSET, &resetWord, sizeof(resetWord));

    // wait for another reset
    delay(PERIOD_MILLIS);

    // store the count as 0 since the reset window elapsed and a total count is being reported
    resetWord = WORD_PATTERN;
    ESP.rtcUserMemoryWrite(RESET_OFFSET, &resetWord, sizeof(resetWord));

    return totalCount;
}

// global definitions
ResetDetectorClass ResetDetector;
