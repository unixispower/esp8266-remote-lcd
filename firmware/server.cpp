#include "server.h"
#include "static.h"
#include "config.h"
#include "eeprom.h"
#include "firmware.h"

#include <Esp.h>

#include <cstdio>
#include <cstring>


// web server settings
const uint16_t WEB_PORT = 80;

// Errors
const char ERROR_405[] = "HTTP method not allowed";


ConfigServer::ConfigServer()
    : webServer(WEB_PORT) {}

void ConfigServer::setup() {
    // register paths and start web server
    this->webServer.on("/firmware", [this]() { this->handleFirmware(); });
    this->webServer.on("/config", [this]() { this->handleConfig(); });
    staticRegisterAll(&this->webServer); // serve all pages in static.h
    this->webServer.begin();
}

void ConfigServer::loop() {
    this->webServer.handleClient();
}

// handle a GET to /firmware
void ConfigServer::handleFirmware() {
    if (this->webServer.method() == HTTP_GET) {
        // build response body
        String body;
        body += "title=";
        body += this->urlEncode(FIRMWARE_TITLE);
        body += "&name=";
        body += this->urlEncode(FIRMWARE_NAME);
        body += "&version=";
        body += this->urlEncode(FIRMWARE_VERSION);
        this->webServer.send(200, "application/x-www-form-urlencoded", body);
    } else {
        this->webServer.send(405, "text/plain", ERROR_405);
    }
}

// handle a GET, POST, or DELETE to /config
void ConfigServer::handleConfig() {
    if (this->webServer.method() == HTTP_GET) {
        // load config from EEPROM
        config_values storedValues;
        EepromStore.getConfig(&storedValues);
        StringConfig stringConfig(&storedValues);

        // build response body
        String body;
        for (size_t i = 0; i < StringConfig::USER_FIELD_COUNT; ++i) {
            // append separator
            if (i != 0) {
                body += "&";
            }

            // append key=value pair
            const char* fieldName = StringConfig::USER_FIELDS[i];
            String formattedValue = stringConfig.format(fieldName);
            body += this->urlEncode(fieldName);
            body += "=";
            body += this->urlEncode(formattedValue.c_str());
        }
        this->webServer.send(200, "application/x-www-form-urlencoded", body);

    } else if (this->webServer.method() == HTTP_POST) {
        config_values postedValues = {};
        StringConfig stringConfig(&postedValues);

        // parse all uploaded fields
        for (size_t i = 0; i < this->webServer.args(); i++) {
            const String key = this->webServer.argName(i);
            const String value = this->webServer.arg(i);

            // skip the ESP8266WebServer generated "plain" field
            if (key == "plain") {
                continue;
            }

            // report parsing error to the web interface
            const char* error = stringConfig.parse(key.c_str(), value.c_str());
            if (error != NULL) {
                this->webServer.send(400, "text/plain", error);
                return;
            }
        }

        // make sure all required fields are set
        const char* error = stringConfig.isValid();
        if (error != NULL) {
            this->webServer.send(400, "text/plain", error);
            return;
        }

        // write parsed fields to EEPROM
        EepromStore.setConfig(&postedValues);
        EepromStore.setMode(MODE_CLIENT);  // set for next boot
        EepromStore.commit();

        this->webServer.send(200);
        this->reboot();

    } else if (this->webServer.method() == HTTP_DELETE) {
        // reset to default configuration
        config_values defaultValues = CONFIG_DEFAULT_VALUES;
        EepromStore.setConfig(&defaultValues);
        EepromStore.setMode(MODE_CONFIG);  // set for next boot
        EepromStore.commit();

        this->webServer.send(200);
        this->reboot();

    } else {
        this->webServer.send(405, "text/plain", ERROR_405);
    }
}

// reboot the device
void ConfigServer::reboot() {
    delay(1000);  // allow time for response to be sent
    ESP.restart();
    delay(1000);  // allow time for loop thread to be killed
}

// URL encode value into a String
String ConfigServer::urlEncode(const char* value) {
    const char URL_RESERVED_CHARS[] = "!#$%&'()*+,/:;=?@[]";

    String stringValue = String();
    char buffer[4];  // %00\0
    for (size_t i = 0, length = strlen(value); i < length; ++i) {
        char character = value[i];

        // make a 1 character string of the current char
        buffer[0] = character;
        buffer[1] = '\0';

        // percent encode the buffer if it contains a reserved character
        if (strstr(URL_RESERVED_CHARS, buffer) != NULL) {
            sprintf(buffer, "%%%02X", character);
        }

        // append the buffer to the output string
        stringValue += buffer;
    }

    return stringValue;
}
