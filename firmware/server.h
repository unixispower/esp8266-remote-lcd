/**
 * Config mode web server.
 */
#ifndef SERVER_H
#define SERVER_H

#include <ESP8266WebServer.h>
#include <WString.h>


/**
 * Config mode web server.
 * This server hosts static web content as well as a config API.
 */
class ConfigServer {
    public:
        ConfigServer();
        void setup();
        void loop();

    private:
        ESP8266WebServer webServer;
        void handleFirmware();
        void handleConfig();
        void reboot();
        String urlEncode(const char* value);
};

#endif
